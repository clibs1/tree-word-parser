#include <stdlib.h>
#include "treeWordParser.h"
#include "clogger.h"

int main(void) {
	tree T = createTree();
	logInfo("created tree");

	enterWord(T, "according", NULL);
	enterWord(T, "to", NULL);
	enterWord(T, "all", NULL);
	enterWord(T, "known", NULL);
	enterWord(T, "laws", NULL);
	enterWord(T, "of", NULL);
	enterWord(T, "aviation", NULL);
	enterWord(T, "there", NULL);
	enterWord(T, "is", NULL);
	enterWord(T, "no", NULL);
	enterWord(T, "way", NULL);
	enterWord(T, "a", NULL);
	enterWord(T, "bee", NULL);
	enterWord(T, "should", NULL);
	enterWord(T, "be", NULL);
	enterWord(T, "able", NULL);
	enterWord(T, "to", NULL);
	enterWord(T, "fly", NULL);
	enterWord(T, "aaa", NULL);
	enterWord(T, "aab", NULL);
	enterWord(T, "aac", NULL);
	enterWord(T, "aad", NULL);
	enterWord(T, "aae", NULL);
	enterWord(T, "aba", NULL);
	enterWord(T, "abb", NULL);
	enterWord(T, "abc", NULL);
	enterWord(T, "bbb", NULL);
	enterWord(T, "bbd", NULL);
	enterWord(T, "bac", NULL);
	enterWord(T, "bca", NULL);
	enterWord(T, "bde", NULL);
	logInfo("inserted words");

	printTree(T);
	logInfo("printed tree");

	unsigned long long wordCount;
	char** words = getWords(T, &wordCount);
	printf("found %llu words\n", wordCount);
	for (unsigned long long i = 0; i < wordCount; i++) {
		printf("found %s\n", words[i]);
		free(words[i]);
	}
	free(words);
	void** leaves = getLeaves(T, &wordCount);
	for (unsigned long long i = 0; i < wordCount; i++) {
		if(leaves[i])
			free(leaves[i]);
	}
	free(leaves);
	freeTree(T);
	T = NULL;
	logInfo("freed tree");
	return EXIT_SUCCESS;
}