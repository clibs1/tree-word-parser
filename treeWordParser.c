/**
	Copyright (C) 2022  Madeline MARIN-PACHE

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
	marinpache.madeline@gmail.com
*/

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include "treeWordParser.h"
#include "clogger.h"

#define ALPHABET_SIZE 27 //26 letters + null byte
#define TREE_ROOT_CHAR 32 //space character

union next
{
	struct treeNode_s** children;
	void (*func)(void*);
};

/**
 * a single node in the tree
 * letter is TREE_ROOT_CHAR -> root of the tree
 * letter is a letter       -> midway node
 * letter is '\0'           -> leaf node
*/
struct treeNode_s
{
	char letter;
	union next next;
};

#define treeNode struct treeNode_s

/*
 * returns a letter id
 * case insensitive
 * returns -1 if character is not a valid letter
*/
char getLetterId(char src) {
	if (src == '\0') {
		return 0;
	}
	else if (src < 65) {
		return -1; //error invalid character
	}
	else if (src < 91) {
		return src - 64; //uppercase letters
	}
	else if (src < 97) {
		return -1;
	}
	else if (src < 123) {
		return src - 96; //lowercase letters
	}
	else {
		return -1;
	}
}

treeNode* createNode(char letter) {
	treeNode* node = malloc(sizeof(treeNode));
	if (node == NULL) {
		log("memory allocation error", CLOG_ERROR);
		exit(EXIT_FAILURE);
	}
	node->letter = letter;
	if (letter != '\0') {
		node->next.children = calloc(
			sizeof(treeNode*),
			ALPHABET_SIZE
		);
		if (node->next.children == NULL) {
			log("memory allocation error", CLOG_ERROR);
			exit(EXIT_FAILURE);
		}
	}
	return node;
}

treeNode* createEndNode(void (*func)(void*)) {
	treeNode* node = createNode('\0');
	node->next.func = func;
	return node;
}

tree createTree() {
	return createNode(TREE_ROOT_CHAR);
}

treeNode* addChildLetter(treeNode* node, char letter) {
	char letterId = getLetterId(letter);
	if (letterId < 0) {
		log("erroneous letter",  CLOG_ERROR);
		exit(EXIT_FAILURE);
	}
	if (node->next.children[letterId] == NULL) {
		node->next.children[letterId] = createNode(letter);
	}
	return node->next.children[letterId];
}

int enterWord(tree T, char* word, void (*func)(void*)) {
	if (strlen(word) > TREE_MAX_WORD_SIZE) {
		logWarn("Word too long to enter in tree");
		return -2;
	}
	char* letter = word;
	while (*letter != '\0') {
		T = addChildLetter(T, *(letter++));
	}
	if (T->next.children[0]) {
		return -1;
	}
	T->next.children[0] = createEndNode(func);
	return 0;
}

//terminal recursive, compile with  at least -02
void* getWord(tree T, char* word) {
	if (*word == '\0') {
		if (T->next.children[0]) {
			//end node for word found, return assiciated function
			return T->next.func;
		}
		else {
			//end of word without end node, word not found
			return NULL;
		}
	}
	else {
		char id = getLetterId(*word);
		if (T->next.children[id]) {
			//next letter in word
			return getWord(T->next.children[id], word + 1);
		}
		else {
			//next letter not in tree, word not found
			return NULL;
		}
	}
}

void getWordsR(
	treeNode* node,
	char*** words,
	char* word,
	unsigned int depth,
	unsigned long long* wordsSize,
	unsigned long long* wordCount
) {
	word[depth] = node->letter;
	if (node->letter == '\0') { // found leaf
		if (*wordCount >= *wordsSize) {
			// expand words array if necessary
			(*wordsSize) += 8;
			*words = reallocarray(*words, *wordsSize, sizeof(char*));
			if (*words == NULL) {
				logError("memory allocation error");
				exit(EXIT_FAILURE);
			}
		}
		// enter word into words array
		(*words)[*wordCount] =
			malloc((strlen(word + 1)) * sizeof(char));
		strcpy((*words)[*wordCount], word + 1);
		(*wordCount)++;
		return;
	}
	// recurse into next nodes
	for (int i = 0; i < ALPHABET_SIZE; i++) {
		if (node->next.children[i]) {
			getWordsR(
				node->next.children[i],
				words,
				word,
				depth + 1,
				wordsSize,
				wordCount
			);
		}
	}
}

char** getWords(tree T, unsigned long long* wordCount) {
	*wordCount = 0;
	unsigned long long wordsSize = 8;
	char** words = malloc(sizeof(char*) * wordsSize);
	char* word = calloc(TREE_MAX_WORD_SIZE + 1, sizeof(char));
	getWordsR(T, &words, word, 0, &wordsSize, wordCount);
	free(word);
	return words;
}

void getLeavesR(
	treeNode* node,
	void*** leaves,
	unsigned long long* leavesSize,
	unsigned long long* leavesCount
) {
	if (node->letter == '\0') { // found leaf
		if (*leavesCount >= *leavesSize) {
			// expand leaves array if necessary
			(*leavesSize) += 8;
			*leaves = reallocarray(*leaves, *leavesSize, sizeof(char*));
			if (leaves == NULL) {
				logError("memory allocation error");
				exit(EXIT_FAILURE);
			}
		}
		// enter leaf into leaves array
		(*leaves)[*leavesCount] = node->next.func;
		(*leavesCount)++;
		return;
	}
	// recurse into next nodes
	for (int i = 0; i < ALPHABET_SIZE; i++) {
		if (node->next.children[i]) {
			getLeavesR(
				node->next.children[i],
				leaves,
				leavesSize,
				leavesCount
			);
		}
	}
}

void** getLeaves(tree T, unsigned long long* leavesCount) {
	*leavesCount = 0;
	unsigned long long leavesSize = 8;
	void** leaves = malloc(sizeof(void*) * leavesSize);
	getLeavesR(T, &leaves, &leavesSize, leavesCount);
	return leaves;
}

//not recursive terminal but depth-first approach should limit issues
void freeNode(treeNode* node) {
	if (node->letter > 0) {
		for (int i = 0; i < ALPHABET_SIZE; i++) {
			if (node->next.children[i]) {
				freeNode(node->next.children[i]);
			}
		}
		free(node->next.children);
	}
	free(node);
}

//terminal recursive, compile with at least -02
// treeNode* removeWord(tree T, char* word) {
// 	if (*word == '\0') {
// 		if (T->next.children[0]) {
// 			treeNode* node = T->next.children[0];
// 			T->next.children[0] = NULL;
// 			return node;
// 		}
// 		else {
// 			//end of word without end node, word not found
// 			return NULL;
// 		}
// 	}
// 	else {
// 		char id = getLetterId(*word);
// 		if (T->next.children[id]) {
// 			//next letter in word
// 			return getWord(T->next.children[id], word + 1);
// 		}
// 		else {
// 			//next letter not in tree, word not found
// 			return NULL;
// 		}
// 	}
// }

//terminal recursive, compile with at least -02
treeNode* getLastSplit(tree T, char* word, treeNode* split) {
	if (*word == '\0') {
		if (T->next.children[0]) {
			return split;
		}
		else {
			//end of word without end node, word not found
			return NULL;
		}
	}
	else {
		char id = getLetterId(*word);
		if (T->next.children[id]) {
			for (char i = 0; i < ALPHABET_SIZE; i++) {
				if (i != id && T->next.children[i]) {
					split = T->next.children[id];
					break;
				}
			}
			//next letter in word
			return getLastSplit(
				T->next.children[id],
				word + 1,
				split
			);
		}
		else {
			//next letter not in tree, word not found
			return NULL;
		}
	}
}

int removeWord(tree T, char* word) {
	treeNode* split = getLastSplit(T, word, T);
	if (split) {
		freeNode(split);
		return 0;
	}
	else {
		//word not found
		return -1;
	}
}

int cleanTree(tree T) {
	log("Function not implemented", CLOG_WARNING);
	return 0;
}

/**
 * offset: indent
 * side: 0 -> <, 1 -> /, 2 -> |, 3 -> \
*/
void printTreeR(treeNode* node, int offset, int side) {
	char line[TREE_MAX_WORD_SIZE * 2 + 3];
	int i;
	for (i = 0; i < offset; i++) {
		line[i] = ' ';
	}
	
	switch (side) {
		case 1:
			line[offset] = '/';
			break;
		case 2:
			line[offset] = '|';
			break;
		case 3:
			line[offset] = '\\';
			break;
		default:
			line[offset] = '<';
	}

	if (node->letter == '\0') {
		line[offset + 1] = '0';
		line[offset + 2] = '\n';
		line[offset + 3] = '\0';
		printf(line);
		return;
	}
	else {
		line[offset + 1] = node->letter;
	}

	int last = 1;
	int first = 0;
	for (i = 1; i < ALPHABET_SIZE; i++) {
		if (node->next.children[i]) {
			last = i;
			if (first == 0) {
				first = i;
			}
		}
	}

	for (i = 1; i < ALPHABET_SIZE; i++) {
		if (node->next.children[i]) {
			if (i == first) {
				printTreeR(node->next.children[i], offset + 2, 1);
			}
			else if (i == last) {
				printTreeR(node->next.children[i], offset + 2, 3);
			}
			else {
				printTreeR(node->next.children[i], offset + 2, 2);
			}
		}
	}

	line[offset + 2] = '\n';
	line[offset + 3] = '\0';
	printf(line);
}

void printTree(tree T) {
	printTreeR(T, 0, 0);
}

void freeTree(tree T) {
	freeNode(T);
}

// void testWord(tree T, char* word, treeNode* node) {
// 	char msg[40];
// 	if (getWord(T, word) == node) {
// 		sprintf(msg, "Success: %s", word);
// 		log(msg, CLOG_DEBUG);
// 	}
// 	else {
// 		sprintf(msg, "Failure: %s", word);
// 		log(msg, CLOG_DEBUG);
// 	}
// }