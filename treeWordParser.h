/**
	Copyright (C) 2022  Madeline MARIN-PACHE

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

	marinpache.madeline@gmail.com
*/

#ifndef __TREE_H__
#define __TREE_H__

/**
 * maximum number of letters in a word
 * not taking null terminator into account
*/
#define TREE_MAX_WORD_SIZE 64

typedef struct treeNode_s* tree;

/**
 * creates a tree structure
*/
tree createTree();

/**
 * takes in a tree and a null terminated string containing a word
 * enters the word in the tree
 * returns  0 if the operation was successfull
 * returns -1 if the word was already in the tree
 * returns -2 if the word was too long
*/
int enterWord(tree T, char* word, void (*func)(void*));

/**
 * takes in a tree and a null terminated string containing a word
 * removes the word from the tree
 * returns  0 if the operation was successfull
 * returns -1 if the word was not in the tree
*/
int removeWord(tree T, char* word);

/**
 * takes in a tree and a null terminated word
 * returns the associated function for that word
 * returns null if the word was not in the tree
*/
void* getWord(tree T, char* word);

/**
 * returns an array containing all words registered in the tree
 * each word will be a null terminated string
 * wordCount will be set to the number of words in the tree
 * if wordCount is null word counting will be ignored
 * traverses the tree in the same order as getLeaves
*/
char** getWords(tree T, unsigned long long* wordCount);

/**
 * returns an array containing all functions pointed to by tree leaves
 * leavesCount will be set to the number of leaves reached
 * if leavesCount is null word counting will be ignored
 * traverses the tree in the same order as getWords
*/
void** getLeaves(tree T, unsigned long long* leavesCount);

/**
 * prints a tree to standard output
*/
void printTree(tree T);

/**
 * frees a tree structure
 * /!\ does not free any of the data pointed to by the leaves
*/
void freeTree(tree T);

#endif